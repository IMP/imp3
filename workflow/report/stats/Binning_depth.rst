Depth of coverage with metagenomic reads for all contigs in the DASTool bins, and/or percentage of genes within the DASTool bins with metatranscriptomic reads mapping.
