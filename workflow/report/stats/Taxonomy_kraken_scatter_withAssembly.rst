Visualization of relative number of reads recruited by kraken2 hits at each analysed taxonomic rank. Hits that were not found in the assembly are depicted by open symbols. 
