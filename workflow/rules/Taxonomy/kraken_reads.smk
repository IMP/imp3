from os.path import getsize
KRAKSIZE = float(os.path.getsize(DBPATH + "/" + config['krakendb'] + "/hash.k2d" )) / 1000000000
KRAKTHREADS = round( (KRAKSIZE / float(BIGMEMCORE[:-1])) + 0.5)


rule kraken_reads:
    input:
        "Preprocessing/{type}.r1.preprocessed.fq",
        "Preprocessing/{type}.r2.preprocessed.fq",
    output:
        "Analysis/taxonomy/kraken/{type}.kraken.output.gz",
        report("Analysis/taxonomy/kraken/{type}.kraken.report",category="Taxonomy")
    resources:
        runtime = "8:00:00",
        mem = BIGMEMCORE
    params:
        tmp_out= lambda wildcards: "Analysis/taxonomy/kraken/" + wildcards.type + ".kraken.output"
    threads: getThreads(KRAKTHREADS) 
    conda: ENVDIR + "/IMP_taxonomy.yaml"
    log: "logs/analysis_kraken_reads.{type}.log"
    message: "kraken_reads: Running kraken2 on {wildcards.type} reads against {config[krakendb]}."
    shell:
	    """
	    mkdir -p Analysis/taxonomy/kraken
	    kraken2 -db {DBPATH}/{config[krakendb]} --threads {threads} --paired --use-names --output {params.tmp_out} --report {output[1]} {input} > {log} 2>&1
            gzip {params.tmp_out}
	    """

