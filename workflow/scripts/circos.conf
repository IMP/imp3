karyotype = data/contigs.ideogram
chromosomes_units = 1
chromosomes_display_default = yes
show_ticks = yes
show_tick_labels = yes

<colors>
rna  = 85,150,210
dna  = 175,70,70
prot = 120,198,121
snps = 8,48,107

complete = 51,51,51
incomplete = 179,179,179
completerrna = 0,100,0
incompleterrna = 162,205,90
protputative = 162,215,163
protunique = 120,198,121

essential = 255,182,193

<<include etc/colors.conf>>
</colors>

<fonts>
<<include etc/fonts.conf>>
</fonts>

<patterns>
<<include etc/patterns.conf>>
</patterns>

<<include etc/housekeeping.conf>>

<ideogram>
  <spacing>
    default = 0.001r
  </spacing>
  radius = 600p
  thickness = 27p
  fill = yes
  stroke_thickness = 0p
  stroke_color = transparent
  <<include ideogram.label.conf>>
</ideogram>


<ticks>
  skip_first_label = yes
  skip_last_label  = no
  radius           = dims(ideogram,radius_outer)
  tick_separation  = 2p
  label_separation = 12p
  label_font = serif_light
  color = black

  <tick>
    spacing = 10000u
    show_label = no
    orientation = out
    size = 8p
    thickness = 1p
  </tick>
  <tick>
    spacing = 50000u
    multiplier = 0.001
    format = %d kb
    show_label = yes
    label_size = 18p
    orientation = out
    size = 14p
    thickness = 2p
  </tick>
</ticks>

<highlights>
  z = 0
  <highlight>
    file = data/essentialGenes.highlights.txt
    fill_color = essential
    r0 = 0.25r
    r1 = 1.05r
  </highlight>
</highlights>

<plots>
  <plot>
    show = yes
    type = tile
    file = data/features.forward.txt
    r0 = 0.86r
    r1 = 0.92r
    z = 1
    layers = 1
    layers_overflow = collapse
    thickness = 36p
    stroke_thickness = 0
    stroke_color = transparent
  </plot>

  <plot> 
    show = yes 
    type = tile 
    file = data/features.reverse.txt 
    r0 = 0.8r 
    r1 = 0.86r 
    z = 1 
    layers = 1 
    layers_overflow = collapse 
    thickness = 36p 
    stroke_thickness = 0 
    stroke_color = transparent 
  </plot>

<plot>
    show = yes
    type = tile
    file = data/rRNAs.forward.txt
    r0 = 0.74r
    r1 = 0.80r
    z = 1
    layers = 1
    layers_overflow = collapse
    thickness = 36p
    stroke_thickness = 0
    stroke_color = transparent
  </plot>

  <plot> 
    show = yes 
    type = tile 
    file = data/rRNAs.reverse.txt 
    r0 = 0.68r 
    r1 = 0.74r 
    z = 1 
    layers = 1 
    layers_overflow = collapse 
    thickness = 36p 
    stroke_thickness = 0 
    stroke_color = transparent 
  </plot>

 <plot>
    show = yes
    type = histogram
    file = data/features.DNAcovGenes.txt
    orientation = out
    fill_under = yes
    fill_color = dna
    color = dna
    thickness = 1
    extend_bin = no
    z = 0
    r0 = 0.57r
    r1 = 0.67r
    min = 0
 #   max = 1.5
 </plot>

 <plot>
    show = yes
    type = histogram
    file = data/features.RNAcovGenes.txt
    orientation = out
    fill_under = yes
    fill_color = rna
    color = rna
    thickness = 1
    extend_bin = no
    z = 0
    r0 = 0.46r
    r1 = 0.56r
    min = 0
 #   max = 1.5
 </plot>

# <plot>
#   show = yes
#    type = histogram
#    file = data/features.proteinGenes.txt
#    orientation = out
#    fill_under = yes
#    thickness = 1
#    extend_bin = no
#    z = 0
#    r0 = 0.25r
#    r1 = 0.35r
#    min = 4
 #   max = 1.5
# </plot>
</plots>


<links>

<link>
file          = data/essentialGenes.links.txt
radius        = 0.22r
color         = essential

# Curves look best when this value is small (e.g. 0.1r or 0r)
bezier_radius = 0r
thickness     = 1

# These parameters have default values. To unset them
# use 'undef'
#crest                = undef
#bezier_radius_purity = undef

# Limit how many links to read from file and draw
#record_limit  = 2000

</link>

</links>

<image>
  dir = .
  file = musttest.png
  svg = yes
  png = no

  # radius of inscribed circle in image
  radius = 1000p

  # by default angle=0 is at 3 o'clock position
  angle_offset      = -90

  auto_alpha_colors = yes
  auto_alpha_steps  = 5

  background = white
</image>
